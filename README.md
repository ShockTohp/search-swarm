# Search Swarm

This is a data obfuscation project. The idea is to feed an endless stream of semi-randomized searches (following link chains briefly, and eventually resetting to a new search) to the internet. 
The theory being that inaccurate information is for purposes of fingerprinting and identification. 


# How Search Swarm works
Search swarm contains a list of common google searches. When you open a browser with it, a tab is dedicated to running these searches randomly and following links briefly
before navigating back to the search page and stating another search chain.

# What Search Swarm doesn't do
 Search swarm is not anonymity tool in and of itself. As all it does is feed the data mining internet with common searches while you browse, it does not prevent you from
exposing your own data. It is designed to be used with your own privacy measures to increase anonymity, not hide completely.

# Installation
### Dependencies
- python3
- make


## Installation Instruction's
Clone this repo and run make install

# Usage
Simply run the command 
```shell 
$ swarm.py [browser] 
```
in a terminal and then browse normally
supported browsers:
- Firefox
- Google Chrome

# Future Development goals
1. Implement a better search generation method
.- Am currently thinking about using a text generator trained on the top 100 google searches each day to generate a list of similar searches
2. Make it run in the background as opposed to a browser window.  
3. Remove selenium dependence
