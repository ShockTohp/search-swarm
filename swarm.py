#!/home/kevin/PythonProjects/SearchSwarm/bin/python
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
import sys

def main(argv):
    if(len(argv) < 2):
        print("Please provide a browser")
        exit()
    elif argv[1] is "Firefox" or "firefox":
        browser = webdriver.Firefox()
    elif (argv[1] is "Chrome" or "chrome"):
        browser = webdriver.Chrome()
    else:
        print("That browser is unsupported")
        exit()
    while(1 is 1):
        search(browser)

def search(browser):
    browser.get('https://www.google.com/')
    assert 'Google' in browser.title

    search_box =  browser.find_element_by_name('q')
    search_box.send_keys("Test" + Keys.RETURN)

main(sys.argv)
